package com.smartsoftasia.library.androidaudiorecorder;

import android.graphics.drawable.ColorDrawable;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.smartsoftasia.library.recordaudio.AudioPlayerView;
import com.smartsoftasia.library.recordaudio.RxAudioRecorder;

import io.reactivex.functions.Consumer;

public class MainActivity extends AppCompatActivity {

  private static final String AUDIO_FILE_PATH =
      Environment.getExternalStorageDirectory() + "/recorded_audio.wav";
  private static final int RECORD_AUDIO = 0;

  private AudioPlayerView mAudioPlayerView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    mAudioPlayerView = (AudioPlayerView) findViewById(R.id.audioplayerview);

    if (getSupportActionBar() != null) {
      getSupportActionBar().setBackgroundDrawable(
          new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));
    }
  }

  public void recordAudio(View v) {
    RxAudioRecorder.with(getApplicationContext())
        .requestAudio(AUDIO_FILE_PATH, ContextCompat.getColor(this, R.color.recorder_bg))
        .subscribe(new Consumer<String>() {
          @Override
          public void accept(String s) throws Exception {
            mAudioPlayerView.setMediaUrl(s);
          }
        });
  }
}